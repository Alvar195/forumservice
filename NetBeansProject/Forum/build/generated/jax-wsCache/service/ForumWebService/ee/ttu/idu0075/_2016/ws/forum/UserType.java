
package ee.ttu.idu0075._2016.ws.forum;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for userType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="premiumMember" type="{http://www.ttu.ee/idu0075/2016/ws/forum}premiumMemberType"/&gt;
 *         &lt;element name="userPosts" type="{http://www.ttu.ee/idu0075/2016/ws/forum}postListType"/&gt;
 *         &lt;element name="userPostCount" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="dateCreated" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userType", propOrder = {
    "id",
    "username",
    "premiumMember",
    "userPosts",
    "userPostCount",
    "dateCreated"
})
public class UserType {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected String username;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PremiumMemberType premiumMember;
    @XmlElement(required = true)
    protected PostListType userPosts;
    @XmlElement(required = true)
    protected BigInteger userPostCount;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateCreated;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the premiumMember property.
     * 
     * @return
     *     possible object is
     *     {@link PremiumMemberType }
     *     
     */
    public PremiumMemberType getPremiumMember() {
        return premiumMember;
    }

    /**
     * Sets the value of the premiumMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link PremiumMemberType }
     *     
     */
    public void setPremiumMember(PremiumMemberType value) {
        this.premiumMember = value;
    }

    /**
     * Gets the value of the userPosts property.
     * 
     * @return
     *     possible object is
     *     {@link PostListType }
     *     
     */
    public PostListType getUserPosts() {
        return userPosts;
    }

    /**
     * Sets the value of the userPosts property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostListType }
     *     
     */
    public void setUserPosts(PostListType value) {
        this.userPosts = value;
    }

    /**
     * Gets the value of the userPostCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUserPostCount() {
        return userPostCount;
    }

    /**
     * Sets the value of the userPostCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUserPostCount(BigInteger value) {
        this.userPostCount = value;
    }

    /**
     * Gets the value of the dateCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCreated() {
        return dateCreated;
    }

    /**
     * Sets the value of the dateCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCreated(XMLGregorianCalendar value) {
        this.dateCreated = value;
    }

}
