
package ee.ttu.idu0075._2016.ws.forum;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2016.ws.forum package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetUserResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/forum", "getUserResponse");
    private final static QName _AddUserResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/forum", "addUserResponse");
    private final static QName _GetPostResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/forum", "getPostResponse");
    private final static QName _AddPostResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/forum", "addPostResponse");
    private final static QName _AddUserPostResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/forum", "addUserPostResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2016.ws.forum
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUserPostListRequest }
     * 
     */
    public GetUserPostListRequest createGetUserPostListRequest() {
        return new GetUserPostListRequest();
    }

    /**
     * Create an instance of {@link GetUserPostListResponse }
     * 
     */
    public GetUserPostListResponse createGetUserPostListResponse() {
        return new GetUserPostListResponse();
    }

    /**
     * Create an instance of {@link PostType }
     * 
     */
    public PostType createPostType() {
        return new PostType();
    }

    /**
     * Create an instance of {@link GetUserListRequest }
     * 
     */
    public GetUserListRequest createGetUserListRequest() {
        return new GetUserListRequest();
    }

    /**
     * Create an instance of {@link GetUserListResponse }
     * 
     */
    public GetUserListResponse createGetUserListResponse() {
        return new GetUserListResponse();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link GetUserRequest }
     * 
     */
    public GetUserRequest createGetUserRequest() {
        return new GetUserRequest();
    }

    /**
     * Create an instance of {@link AddUserRequest }
     * 
     */
    public AddUserRequest createAddUserRequest() {
        return new AddUserRequest();
    }

    /**
     * Create an instance of {@link GetPostListRequest }
     * 
     */
    public GetPostListRequest createGetPostListRequest() {
        return new GetPostListRequest();
    }

    /**
     * Create an instance of {@link GetPostListResponse }
     * 
     */
    public GetPostListResponse createGetPostListResponse() {
        return new GetPostListResponse();
    }

    /**
     * Create an instance of {@link GetPostRequest }
     * 
     */
    public GetPostRequest createGetPostRequest() {
        return new GetPostRequest();
    }

    /**
     * Create an instance of {@link AddPostRequest }
     * 
     */
    public AddPostRequest createAddPostRequest() {
        return new AddPostRequest();
    }

    /**
     * Create an instance of {@link AddUserPostRequest }
     * 
     */
    public AddUserPostRequest createAddUserPostRequest() {
        return new AddUserPostRequest();
    }

    /**
     * Create an instance of {@link PostListType }
     * 
     */
    public PostListType createPostListType() {
        return new PostListType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/forum", name = "getUserResponse")
    public JAXBElement<UserType> createGetUserResponse(UserType value) {
        return new JAXBElement<UserType>(_GetUserResponse_QNAME, UserType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/forum", name = "addUserResponse")
    public JAXBElement<UserType> createAddUserResponse(UserType value) {
        return new JAXBElement<UserType>(_AddUserResponse_QNAME, UserType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/forum", name = "getPostResponse")
    public JAXBElement<PostType> createGetPostResponse(PostType value) {
        return new JAXBElement<PostType>(_GetPostResponse_QNAME, PostType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/forum", name = "addPostResponse")
    public JAXBElement<PostType> createAddPostResponse(PostType value) {
        return new JAXBElement<PostType>(_AddPostResponse_QNAME, PostType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/forum", name = "addUserPostResponse")
    public JAXBElement<PostType> createAddUserPostResponse(PostType value) {
        return new JAXBElement<PostType>(_AddUserPostResponse_QNAME, PostType.class, null, value);
    }

}
