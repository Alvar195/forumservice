
package ee.ttu.idu0075._2016.ws.forum;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ForumPortType", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ForumPortType {


    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.GetUserPostListResponse
     */
    @WebMethod
    @WebResult(name = "getUserPostListResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public GetUserPostListResponse getUserPostList(
        @WebParam(name = "getUserPostListRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        GetUserPostListRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.GetUserListResponse
     */
    @WebMethod
    @WebResult(name = "getUserListResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public GetUserListResponse getUserList(
        @WebParam(name = "getUserListRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        GetUserListRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.UserType
     */
    @WebMethod
    @WebResult(name = "getUserResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public UserType getUser(
        @WebParam(name = "getUserRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        GetUserRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.UserType
     */
    @WebMethod
    @WebResult(name = "addUserResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public UserType addUser(
        @WebParam(name = "addUserRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        AddUserRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.GetPostListResponse
     */
    @WebMethod
    @WebResult(name = "getPostListResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public GetPostListResponse getPostList(
        @WebParam(name = "getPostListRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        GetPostListRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.PostType
     */
    @WebMethod
    @WebResult(name = "getPostResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public PostType getPost(
        @WebParam(name = "getPostRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        GetPostRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.PostType
     */
    @WebMethod
    @WebResult(name = "addPostResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public PostType addPost(
        @WebParam(name = "addPostRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        AddPostRequest parameter);

    /**
     * 
     * @param parameter
     * @return
     *     returns ee.ttu.idu0075._2016.ws.forum.PostType
     */
    @WebMethod
    @WebResult(name = "addUserPostResponse", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
    public PostType addUserPost(
        @WebParam(name = "addUserPostRequest", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", partName = "parameter")
        AddUserPostRequest parameter);

}
