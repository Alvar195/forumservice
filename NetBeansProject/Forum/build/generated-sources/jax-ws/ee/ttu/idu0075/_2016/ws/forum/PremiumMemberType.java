
package ee.ttu.idu0075._2016.ws.forum;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for premiumMemberType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="premiumMemberType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NO"/&gt;
 *     &lt;enumeration value="YES"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "premiumMemberType")
@XmlEnum
public enum PremiumMemberType {

    NO,
    YES;

    public String value() {
        return name();
    }

    public static PremiumMemberType fromValue(String v) {
        return valueOf(v);
    }

}
