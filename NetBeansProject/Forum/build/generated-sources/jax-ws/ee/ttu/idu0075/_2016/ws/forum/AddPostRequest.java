
package ee.ttu.idu0075._2016.ws.forum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="API_TOKEN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postTitle" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apitoken",
    "token",
    "postTitle",
    "postMessage",
    "postDate"
})
@XmlRootElement(name = "addPostRequest")
public class AddPostRequest {

    @XmlElement(name = "API_TOKEN", required = true)
    protected String apitoken;
    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String postTitle;
    @XmlElement(required = true)
    protected String postMessage;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar postDate;

    /**
     * Gets the value of the apitoken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPITOKEN() {
        return apitoken;
    }

    /**
     * Sets the value of the apitoken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPITOKEN(String value) {
        this.apitoken = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the postTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostTitle() {
        return postTitle;
    }

    /**
     * Sets the value of the postTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostTitle(String value) {
        this.postTitle = value;
    }

    /**
     * Gets the value of the postMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostMessage() {
        return postMessage;
    }

    /**
     * Sets the value of the postMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostMessage(String value) {
        this.postMessage = value;
    }

    /**
     * Gets the value of the postDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostDate() {
        return postDate;
    }

    /**
     * Sets the value of the postDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostDate(XMLGregorianCalendar value) {
        this.postDate = value;
    }

}
