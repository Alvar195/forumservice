/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.forum;

import ee.ttu.idu0075._2016.ws.forum.*;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 * REST Web Service
 *
 * @author alvar
 */
@Path("users")
public class UsersResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UsersResource() {
    }
    
    
    //getUserPostList
    //addUserPost
    
    @GET
    @Path("all/{premiumMember}/{dateCreated}")
    @Produces("application/json")
    public GetUserListResponse getUserList (
            @PathParam("premiumMember") String premiumMember,
            @PathParam("dateCreated") String dateCreated,
            @QueryParam("hasPosts") String hasPosts,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN){
        ForumWebService ws = new ForumWebService();
        GetUserListRequest request = new GetUserListRequest();
                
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        request.setHasPosts(hasPosts);
        
        PremiumMemberType premium = (premiumMember.equals("YES")) ? PremiumMemberType.YES : PremiumMemberType.NO;
        request.setPremiumMember(premium);

        try {
            request.setDateCreated(DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreated));
        } catch (DatatypeConfigurationException ex) {}
        return ws.getUserList(request);
    }
    
    
    @GET
    @Path("user/{id : \\d+}")
    @Produces("application/json")
    public UserType getUser(
            @PathParam("id") int id,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        
        ForumWebService ws = new ForumWebService();
        GetUserRequest request = new GetUserRequest();
        request.setId(BigInteger.valueOf(id));
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        return ws.getUser(request);
    }
    
    @GET
    @Path("user/posts/{id : \\d+}")
    @Produces("application/json")
    public GetUserPostListResponse getUserPostList(
            @PathParam("id") int id,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        
        ForumWebService ws = new ForumWebService();
        GetUserPostListRequest request = new GetUserPostListRequest();
        request.setUserId(BigInteger.valueOf(id));
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        return ws.getUserPostList(request);
    }
    

    @POST
    @Path("add")
    @Consumes("application/json")
    @Produces("application/json")
    public UserType addUser(
            @QueryParam("username") String username,
            @QueryParam("dateCreated") String dateCreated,
            @QueryParam("premiumMember") String premiumMember,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        ForumWebService ws = new ForumWebService();
        AddUserRequest request = new AddUserRequest();
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        request.setUsername(username);
        PremiumMemberType premium = (premiumMember.equals("YES")) ? PremiumMemberType.YES : PremiumMemberType.NO;
        request.setPremiumMember(premium);
        try {
            request.setDateCreated(DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreated));
        } catch (DatatypeConfigurationException ex) {}

        return ws.addUser(request);
    }
    
    @POST
    @Path("link")
    @Consumes("application/json")
    @Produces("application/json")
    public PostType addUserPost(
            @QueryParam("userId") String userId,
            @QueryParam("postId") String postId,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        ForumWebService ws = new ForumWebService();
        AddUserPostRequest request = new AddUserPostRequest();
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        request.setUserId(BigInteger.valueOf(Integer.parseInt(userId)));
        request.setPostId(BigInteger.valueOf(Integer.parseInt(postId)));
        
        return ws.addUserPost(request);
    }
}
