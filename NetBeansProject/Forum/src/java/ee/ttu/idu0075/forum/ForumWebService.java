/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.forum;

import ee.ttu.idu0075._2016.ws.forum.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author alvar
 */
@WebService(serviceName = "ForumService", portName = "ForumPort", endpointInterface = "ee.ttu.idu0075._2016.ws.forum.ForumPortType", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/forum", wsdlLocation = "WEB-INF/wsdl/ForumWebService/forum.wsdl")
public class ForumWebService {

    public static BigInteger nextProductId = new BigInteger("0");
    public static BigInteger nextUserId = new BigInteger("0");
    static List<PostType> postList = new ArrayList<PostType>();
    static List<UserType> userList = new ArrayList<UserType>();

    public GetUserPostListResponse getUserPostList(GetUserPostListRequest parameter) {
        
        GetUserPostListResponse response = new GetUserPostListResponse();
        UserType userType = null;
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            for (int i = 0; i < userList.size(); i++) {
                if(userList.get(i).getId().compareTo(parameter.getUserId()) == 0) {
                    userType = userList.get(i);
                    break; //No need to guarantee O(n)
                }
            }
            
            for(PostType postType : userType.getUserPosts().getPostList()) {
                response.getUserPostList().add(postType);
            }
        }
        return response;
        
    }

    public GetUserListResponse getUserList(GetUserListRequest parameter) {
        GetUserListResponse response = new GetUserListResponse();
        if(parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            boolean validPremium = false;
            boolean validHasPosts = false;
            for (UserType userType : userList) {
                
                //filter correct premium member results - required
                validPremium = (parameter.getPremiumMember().equals(userType.getPremiumMember()));
                
                //filter user has posts - optional
                if(parameter.getHasPosts() != null && parameter.getHasPosts().equals("YES")) {
                    validHasPosts = (userType.getUserPostCount().compareTo(new BigInteger("0")) == 1);
                }else {
                    validHasPosts = true;
                }
                //apply filters
                if(validPremium && validHasPosts) response.getUserList().add(userType);
            }
        }
        return response;
    }

    public UserType getUser(GetUserRequest parameter) {
        UserType userType = null;
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            for (int i = 0; i < userList.size(); i++) {
                if(userList.get(i).getId().compareTo(parameter.getId()) == 0) {
                    userType = userList.get(i);
                    break; //No need to guarantee O(n)
                }
            }
        }
        return userType;
    }

    public UserType addUser(AddUserRequest parameter) {
        
        UserType userType = null;
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            BigInteger userPostCount = new BigInteger("0");
            userType = new UserType();
            userType.setId(nextUserId);
            userType.setUsername(parameter.getUsername());
            userType.setUserPostCount(userPostCount);
            userType.setDateCreated(parameter.getDateCreated());
            userType.setUserPosts(new PostListType());
            userType.setPremiumMember(parameter.getPremiumMember());
            userList.add(userType);
            //nextUserId++
            nextUserId = nextUserId.add(BigInteger.ONE);
        }
        return userType;
    }

    public GetPostListResponse getPostList(GetPostListRequest parameter) {
        
        GetPostListResponse response = new GetPostListResponse();
        if(parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            for (PostType postType : postList) {
                response.getPostList().add(postType);
            }
        }
        return response;
    }

    public PostType getPost(GetPostRequest parameter) {
        
        PostType postType = null;
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            for (int i = 0; i < postList.size(); i++) {
                if(postList.get(i).getId().compareTo(parameter.getId()) == 0) {
                    postType = postList.get(i);
                    break;
                }
            }
        }
        return postType;
    }

    public PostType addPost(AddPostRequest parameter) {
        
        PostType postType = null;
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            postType = new PostType();
            postType.setId(nextProductId);
            postType.setPostTitle(parameter.getPostTitle());
            postType.setPostMessage(parameter.getPostMessage());
            postType.setPostDate(parameter.getPostDate());
            postList.add(postType);
            //nextProductId++
            nextProductId = nextProductId.add(BigInteger.ONE);
        }
        return postType;
    }

    public PostType addUserPost(AddUserPostRequest parameter) {
        
        PostType postType = null;
        //Here I'll find the UserType of the user I'm gonna link the video Id to
        if (parameter.getToken().equalsIgnoreCase("uniquerandomid") && parameter.getAPITOKEN().equalsIgnoreCase("salajane")) {
            UserType userType = null;
            for (int i = 0; i < userList.size(); i++) {
                if(userList.get(i).getId().compareTo(parameter.getUserId()) == 0) {
                    userType = userList.get(i);
                    break;
                }
            }
            PostListType postListType = userType.getUserPosts();
            //Here I'll find the PostType of the post I'm gonna link the user Id to
            for (int i = 0; i < postList.size(); i++) {
                if(postList.get(i).getId().compareTo(parameter.getPostId()) == 0) {
                    postType = postList.get(i);
                    break;
                }
            }
            
            //check if post is already linked
            if(!postListType.getPostList().contains(postType)) {
                //Add 1 to userPostCount
                BigInteger userPostCount = userType.getUserPostCount();
                BigInteger newUserPostCount = userPostCount.add(BigInteger.ONE);
                userType.setUserPostCount(newUserPostCount);

                postListType.getPostList().add(postType);
            }
            
        }

        return postType;
        
        
    }
    
}
