# Veebiteenused #

## TTU Veebiteenused projekt "Foorum" ##

Alvar Sokk 142798

### JavaFiles ###
Java failid eraldi

### NetBeansProject ###
Terve NetBeans Projekt

### Forum Dokumentatsioon Alvar Sokk - 142798 ###
Dokumentatsioon

### Forum.postman_collection ###
Postman-i queryd ja testid

### Forum.postman_test_run ###
Postman-i testide tulemused

### forum.wsdl ###
Projekti WSDL

### ForumService-wsdl-soapui-project ###
SoapUI projekt koos testidega