/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.forum;

import ee.ttu.idu0075._2016.ws.forum.*;
import java.math.BigInteger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 *
 * @author alvar
 */
public class Client {
    
    public static void main(String[] args) {
        
        //random date
        String date = "2016-01-01";
        
        ForumWebService forum = new ForumWebService();
        
        //create new User data
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setToken("uniquerandomid");
        addUserRequest.setAPITOKEN("salajane");
        addUserRequest.setUsername("Bob");
        addUserRequest.setPremiumMember(PremiumMemberType.YES);
        try {
            addUserRequest.setDateCreated(DatatypeFactory.newInstance().newXMLGregorianCalendar(date));
        } catch (DatatypeConfigurationException ex) {}
        
        //create new User data
        AddUserRequest addUserRequest2 = new AddUserRequest();
        addUserRequest2.setToken("uniquerandomid");
        addUserRequest2.setAPITOKEN("salajane");
        addUserRequest2.setUsername("Mary");
        addUserRequest2.setPremiumMember(PremiumMemberType.NO);
        try {
            addUserRequest2.setDateCreated(DatatypeFactory.newInstance().newXMLGregorianCalendar(date));
        } catch (DatatypeConfigurationException ex) {}
        
        //add users
        forum.addUser(addUserRequest);
        forum.addUser(addUserRequest2);
        
        //create post data
        AddPostRequest addPostRequest = new AddPostRequest();
        
        addPostRequest.setToken("uniquerandomid");
        addPostRequest.setAPITOKEN("salajane");
        addPostRequest.setPostTitle("This is a post title");
        addPostRequest.setPostMessage("This is the body of the message");
        try {
            addPostRequest.setPostDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(date));
        } catch (DatatypeConfigurationException ex) {}
        
        //add post
        forum.addPost(addPostRequest);
        
        
        //create user and post link
        //this is manual since there isn't an account system implemented (way to determine if user is logged in)
        AddUserPostRequest addUserPostRequest = new AddUserPostRequest();
        
        addUserPostRequest.setToken("uniquerandomid");
        addUserPostRequest.setAPITOKEN("salajane");
        addUserPostRequest.setUserId(BigInteger.ZERO); // id of first user
        addUserPostRequest.setPostId(BigInteger.ZERO); //id of first post
        
        //add post and user link
        forum.addUserPost(addUserPostRequest);
        
        //*Bob registered a new account and created a post which was linked to his account*
        //*Mary registered a new account*
        
        //*Mary wants to see what accounts are on the Forum*
        //*That are created on 2016-01-01 and have posts*
        //*And also is premium account*
        
        //create new users list
        GetUserListRequest getUserListRequest = new GetUserListRequest();
        
        getUserListRequest.setToken("uniquerandomid");
        getUserListRequest.setAPITOKEN("salajane");
        try {
            getUserListRequest.setDateCreated(DatatypeFactory.newInstance().newXMLGregorianCalendar(date));
        } catch (DatatypeConfigurationException ex) {}
        getUserListRequest.setHasPosts("YES");
        getUserListRequest.setPremiumMember(PremiumMemberType.YES);
        getUserListRequest.setHasPosts("YES");
        
        //search with predefined parameters
        GetUserListResponse usersThatMatchSearch = new GetUserListResponse();
        usersThatMatchSearch = forum.getUserList(getUserListRequest);
        
        //Where there any results?
        //There was - Bob with Id:0
        if(usersThatMatchSearch.getUserList().size() > 0) {
            for (UserType user : usersThatMatchSearch.getUserList()) {
                System.out.println(
                        "Account id is " + user.getId() + "\n" +
                        "Account username is " + user.getUsername() + "\n" +
                        "Account has " + user.getUserPostCount() + " post(s)" + "\n"
                        );
                break; //one result is enough
            }
        }
        
        //Should print 
        /*
        Account id is 0
        Account username is Bob
        Account has 1 post(s)
        */    
        
        
        
    }
    
}
