/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.forum;

import ee.ttu.idu0075._2016.ws.forum.*;
import java.math.BigInteger;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * REST Web Service
 *
 * @author alvar
 */
@Path("posts")
public class PostsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PostsResource
     */
    public PostsResource() {
    }

    @GET
    @Path("test")
    @Produces("text/html")
    public String test() {
        return "test";
    }
    
    @GET
    @Path("all")
    @Produces("application/json")
    public GetPostListResponse getPostList (
            @QueryParam("id") String id,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN){
        ForumWebService ws = new ForumWebService();
        GetPostListRequest request = new GetPostListRequest();
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        return ws.getPostList(request);
    }
    

    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public PostType getPost(
            @PathParam("id") int id,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        ForumWebService ws = new ForumWebService();
        GetPostRequest request = new GetPostRequest();
        request.setId(BigInteger.valueOf(id));
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        return ws.getPost(request);
    }
    
    
    @POST
    @Path("add")
    @Consumes("application/json")
    @Produces("application/json")
    public PostType addPost(
            @QueryParam("postTitle") String postTitle,
            @QueryParam("postMessage") String postMessage,
            @QueryParam("postDate") String postDate,
            @QueryParam("token") String token,
            @QueryParam("API_TOKEN") String API_TOKEN) {
        ForumWebService ws = new ForumWebService();
        AddPostRequest request = new AddPostRequest();
        request.setToken(token);
        request.setAPITOKEN(API_TOKEN);
        request.setPostTitle(postTitle);
        request.setPostMessage(postMessage);
        
        try {
            request.setPostDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(postDate));
        } catch (DatatypeConfigurationException ex) {}

        return ws.addPost(request);
    }
}
